#!/usr/bin/env python3
import os
import time

import pandas as pd
import numpy as np
from numpy import nan as NaN


import openpyxl
from openpyxl.styles import Font
from openpyxl.styles.borders import Border, Side

# --------------------------------------------------------------------
# Functions
# --------------------------------------------------------------------
vLeft = lambda x: x[np.arange(np.size(x)//2)]
vRight = lambda x: x[np.arange(np.size(x)%2+np.size(x)//2,np.size(x))]
vMid = lambda x: x[np.size(x)//2] if np.size(x)%2 else NaN

def subsubelem(df):
    rc = df.values.flatten()
    center = vMid(rc)
    other = np.concatenate((vLeft(rc), vRight(rc)))
    return(center, other)

def subsplit(df):
    r2c2 = df.iloc[np.arange(0,3)+(2-2),np.arange(0,3)+(2-2)]
    r5c2 = df.iloc[np.arange(0,3)+(5-2),np.arange(0,3)+(2-2)]
    r8c2 = df.iloc[np.arange(0,3)+(8-2),np.arange(0,3)+(2-2)]

    r2c5 = df.iloc[np.arange(0,3)+(2-2),np.arange(0,3)+(5-2)]
    r5c5 = df.iloc[np.arange(0,3)+(5-2),np.arange(0,3)+(5-2)] # Center
    r8c5 = df.iloc[np.arange(0,3)+(8-2),np.arange(0,3)+(5-2)]

    r2c8 = df.iloc[np.arange(0,3)+(2-2),np.arange(0,3)+(8-2)]
    r5c8 = df.iloc[np.arange(0,3)+(5-2),np.arange(0,3)+(8-2)]
    r8c8 = df.iloc[np.arange(0,3)+(8-2),np.arange(0,3)+(8-2)]

    r = subsubelem(r5c5)
    z = zip(r[1], [r2c2, r5c2, r8c2, r2c5, r8c5, r2c8, r5c8, r8c8])

    return (r2c2, r5c2, r8c2, r2c5, r5c5, r8c5, r2c8, r5c8, r8c8, r[0], z)



def reformat_excel(fp):
    wb = openpyxl.load_workbook(fp)
    sheet = wb.active

    sheet.row_dimensions[0].height = 4
    sheet.row_dimensions[1].height = 20
    sheet.row_dimensions[2].height = 60
    sheet.row_dimensions[3].height = 60
    sheet.row_dimensions[4].height = 60
    sheet.row_dimensions[5].height = 60
    sheet.row_dimensions[6].height = 60
    sheet.row_dimensions[7].height = 60
    sheet.row_dimensions[8].height = 60
    sheet.row_dimensions[9].height = 60
    sheet.row_dimensions[10].height = 60

    sheet.column_dimensions['A'].width = 3
    sheet.column_dimensions['B'].width = 20
    sheet.column_dimensions['C'].width = 20
    sheet.column_dimensions['D'].width = 20
    sheet.column_dimensions['E'].width = 20
    sheet.column_dimensions['F'].width = 20
    sheet.column_dimensions['G'].width = 20
    sheet.column_dimensions['H'].width = 20
    sheet.column_dimensions['I'].width = 20
    sheet.column_dimensions['J'].width = 20

    # set the size of the cell to 24
    sheet.cell(row=6, column=6).font = Font(size=28, bold=True)
    sheet.cell(row=5, column=5).font = Font(size=20, bold=True)

    sheet.cell(row=5, column=5).border = Border(left=Side(style='thick'),
                                                top=Side(style='thick'))

    sheet.cell(row=5, column=6).font = Font(size=20, bold=True)
    sheet.cell(row=5, column=7).font = Font(size=20, bold=True)
    sheet.cell(row=6, column=5).font = Font(size=20, bold=True)
    sheet.cell(row=6, column=7).font = Font(size=20, bold=True)
    sheet.cell(row=7, column=5).font = Font(size=20, bold=True)
    sheet.cell(row=7, column=6).font = Font(size=20, bold=True)
    sheet.cell(row=7, column=7).font = Font(size=20, bold=True)

    sheet.cell(row=3, column=3).font = Font(size=18, bold=True)
    sheet.cell(row=6, column=3).font = Font(size=18, bold=True)
    sheet.cell(row=9, column=3).font = Font(size=18, bold=True)
    sheet.cell(row=3, column=6).font = Font(size=18, bold=True)
    sheet.cell(row=9, column=6).font = Font(size=18, bold=True)
    sheet.cell(row=3, column=9).font = Font(size=18, bold=True)
    sheet.cell(row=6, column=9).font = Font(size=18, bold=True)
    sheet.cell(row=9, column=9).font = Font(size=18, bold=True)

    wb.save(fp)

# --------------------------------------------------------------------
# Main
# --------------------------------------------------------------------
fnr = input("Provide full path of input file: ") or 'null'
fnw = input("Provide full path of output file: ") or 'null'

if (fnr != 'null'):

    print(os.path.realpath(fnr))

    if (fnw == 'null'):
        fnr_ = os.path.splitext(fnr)
        fnw = fnr_[0] + '_out' + fnr_[1]
        #print(fnw)

        dfs = pd.read_excel(os.path.realpath(fnr),
                    sheet_name=None,
                    index=False,
                    header=None)
        for key in dfs.keys():
            # do something
            if len(dfs) > 1:
                fnw = os.path.join(os.path.dirname(fnr), ''.join(key) + '_out.xlsx')

            df = dfs[key]
            sn = 'Sheet1'

            dfr = df.iloc[1:df.shape[0], 1:df.shape[1]]
            subs = subsplit(dfr); # assumes NaN from row1 and column1 removed
            r2c2, r5c2, r8c2, r2c5, r5c5, r8c5, r2c8, r5c8, r8c8, r, z = subs;

            print("\n")
            print(r)
            for x,y in z:
                c,o = subsubelem(y)
                print('|\n|--', c)
                [ print('|   |--', yy) for yy in o ]

            # Check
            df1 = pd.concat([pd.Series(np.arange(dfr.shape[1])*NaN), dfr], axis=1)
            df2 = pd.concat([pd.concat([r2c2, r5c2, r8c2], axis=0),
                             pd.concat([r2c5, r5c5, r8c5], axis=0),
                             pd.concat([r2c8, r5c8, r8c8], axis=0)], axis=1)
            dfw = pd.concat([pd.Series(np.arange(9)*NaN),
                             df2], axis=1)
            print(fnw)
            df.to_excel(fnw,
                        sheet_name=sn,
                        index=False,
                        header=None)

            time.sleep(2.0) # Wait 2 seconds
            reformat_excel(fnw)
